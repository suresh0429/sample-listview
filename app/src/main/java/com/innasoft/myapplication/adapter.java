package com.innasoft.myapplication;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class adapter extends RecyclerView.Adapter<adapter.Holder> {
    List<OnGoingTaskResponse> dataBeanList;
    Context context;
    int row_index = -1;

    public adapter(List<OnGoingTaskResponse> dataBeanList, FragmentActivity activity) {
        this.dataBeanList=dataBeanList;
        this.context=activity;
    }

    @NonNull
    @Override
    public adapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ongoing, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final adapter.Holder holder, final int position) {

       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               row_index=position;
               notifyDataSetChanged();

           }
       });

        if(row_index==position){
            holder.imageView.setVisibility(View.GONE);
            holder.imageView1.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.imageView.setVisibility(View.VISIBLE);
            holder.imageView1.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView txt_area;
        ImageView imageView,imageView1;

        public Holder(@NonNull View itemView) {
            super(itemView);

            imageView=itemView.findViewById(R.id.img1);
            imageView1=itemView.findViewById(R.id.img2);
            txt_area=itemView.findViewById(R.id.txtone);


        }
    }
}
