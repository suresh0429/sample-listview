package com.innasoft.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {
List<OnGoingTaskResponse> onGoingTaskResponseList = new ArrayList<>();
RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        recyclerView = findViewById(R.id.recyclerOngoing);

        adapter adapter = new adapter(onGoingTaskResponseList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(adapter);
        data();
    }

    private void data(){

        OnGoingTaskResponse onGoingTaskResponse = new OnGoingTaskResponse("one","two","3","4");
        onGoingTaskResponseList.add(onGoingTaskResponse);

        onGoingTaskResponse = new OnGoingTaskResponse("one1","two","3","4");
        onGoingTaskResponseList.add(onGoingTaskResponse);

        onGoingTaskResponse = new OnGoingTaskResponse("one1","two","3","4");
        onGoingTaskResponseList.add(onGoingTaskResponse);

        onGoingTaskResponse = new OnGoingTaskResponse("one1","two","3","4");
        onGoingTaskResponseList.add(onGoingTaskResponse);
    }
}
